package com.example.ali.todolist.adapter;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ali.todolist.R;
import com.example.ali.todolist.model.Task;
import com.example.ali.todolist.view.activities.MainActivity;
import com.example.ali.todolist.view_model.SharedTaskViewModel;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapterTask extends RecyclerView.Adapter<RecyclerViewAdapterTask.TaskViewHolder> {

    public static List<Task> tasks;
    public Context activityContext;

    public RecyclerViewAdapterTask(Context activityContext) {
        this.tasks = new ArrayList<>();
        this.activityContext = activityContext;
    }

    public void setOffers(@NonNull List<Task> tasks) {
        this.tasks = tasks;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item, parent, false);

        return new TaskViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder productViewHolder, int position) {
        productViewHolder.taskDescription.setText(tasks.get(position).taskDescription);
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public class TaskViewHolder extends RecyclerView.ViewHolder {
        public TextView taskDescription;

        public TaskViewHolder(final View view) {
            super(view);
            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle("Delete entry");
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SharedTaskViewModel sharedTaskViewModel = ViewModelProviders.of((MainActivity) activityContext).
                                    get(SharedTaskViewModel.class);
                            List<Task> taskList = sharedTaskViewModel.getTaskList().getValue();
                            taskList.remove(getAdapterPosition());
                            sharedTaskViewModel.taskLiveData.setValue(taskList);

                            notifyItemRemoved(getAdapterPosition());
                            notifyItemRangeChanged(getAdapterPosition(), tasks.size());
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });


                    AlertDialog dialog = builder.create();
                    dialog.show();
                    return false;
                }
            });
            taskDescription = view.findViewById(R.id.task_description);
        }
    }
}
