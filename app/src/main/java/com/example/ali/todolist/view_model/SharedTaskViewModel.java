package com.example.ali.todolist.view_model;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.ali.todolist.model.Task;

import java.util.List;

public class SharedTaskViewModel extends ViewModel {
    public MutableLiveData<List<Task>> taskLiveData;

    public SharedTaskViewModel() {
        taskLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<List<Task>> getTaskList() {
        return taskLiveData;
    }
}
