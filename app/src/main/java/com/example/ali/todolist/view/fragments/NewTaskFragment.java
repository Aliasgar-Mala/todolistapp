package com.example.ali.todolist.view.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.ali.todolist.R;
import com.example.ali.todolist.view_model.SharedTaskViewModel;
import com.example.ali.todolist.model.Task;
import com.example.ali.todolist.view.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class NewTaskFragment extends Fragment {

    private EditText taskEditText;
    SharedTaskViewModel sharedTaskViewModel;
    public static NewTaskFragment newInstance() {
        return new NewTaskFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_task_fragment, container, false);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        setHasOptionsMenu(true);

        taskEditText = view.findViewById(R.id.editText_description);

        MainActivity activity = (MainActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sharedTaskViewModel = ViewModelProviders.of(getActivity()).get(SharedTaskViewModel.class);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(R.menu.task_menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                getActivity().onBackPressed();
                break;

            case R.id.action_done:
                getActivity().onBackPressed();
                Task task = new Task();
                String description = taskEditText.getText().toString();
                if (description != null && !description.isEmpty()) {
                    task.taskDescription = description;
                    List<Task> taskList = sharedTaskViewModel.getTaskList().getValue();
                    if (taskList == null) {
                        taskList = new ArrayList<>();
                    }
                    taskList.add(task);
                    sharedTaskViewModel.taskLiveData.setValue(taskList);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
