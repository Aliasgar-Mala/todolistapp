package com.example.ali.todolist.view.fragments;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ali.todolist.R;
import com.example.ali.todolist.adapter.RecyclerViewAdapterTask;
import com.example.ali.todolist.view_model.SharedTaskViewModel;
import com.example.ali.todolist.model.Task;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListOfTaskFragment extends Fragment {

    SharedTaskViewModel sharedTaskViewModel;
    private RecyclerView recyclerView;


    public ListOfTaskFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_of_task, container, false);

        recyclerView = view.findViewById(R.id.task_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(view.getContext(),
                DividerItemDecoration.VERTICAL));

        final RecyclerViewAdapterTask adapter = new RecyclerViewAdapterTask(getActivity());
        recyclerView.setAdapter(adapter);

        sharedTaskViewModel = ViewModelProviders.of(getActivity()).get(SharedTaskViewModel.class);
        sharedTaskViewModel.getTaskList().observe(this, new Observer<List<Task>>() {
            @Override
            public void onChanged(@Nullable List<Task> offersItems) {
                adapter.setOffers(offersItems);
            }
        });

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewTaskFragment newTaskFragment = NewTaskFragment.newInstance();
                getFragmentManager().beginTransaction()
                        .add(R.id.fragment_container, newTaskFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        return view;
    }



}
