package com.example.ali.todolist.view.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.ali.todolist.R;
import com.example.ali.todolist.view.fragments.ListOfTaskFragment;

public class MainActivity extends AppCompatActivity {
    private ListOfTaskFragment listOfTaskFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listOfTaskFragment = new ListOfTaskFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, listOfTaskFragment)
                .commit();
    }
}
